# CoursePlus
[Admin](http://demo.foxthemes.net/courseplus/admin/index.html) Learning [CoursePlus](http://demo.foxthemes.net/courseplus/) web html template.

## Screen shots
![](screen_shots/dashboard.png)

![](screen_shots/book.png)

![](screen_shots/courses.png)

![](screen_shots/setting.png)

![](screen_shots/students.png)

![](screen_shots/login.png)